# IPC defiinition

## Topic & message protocol
There all message is JSON and that must using UTF-8 encoding.

    chat.privmsg {
        time,   // epoch time (integer)
        type,   // 0
        channel, // ex. "#elfinlazz"
        nick,   // ex. "envi"
        what, // message (ex. "message")
    }
    
    channel.joined {
        time,   // epoch time
        type,   // 1
        channel,
        nick
    }
    
    channel.parted {
        time,   // epoch time
        type,   // 2
        channel,
        nick
    }
    
    irc.quit {
        time,   // epoch time
        type,   // 3
        channel,
        nick
    }
    
    public.notify {
        target_channels = []
        what
    }

## Publisher and subscriber definition
### chat.privmsg
* Publisher: IRC client, logviewer-backend
* Subscriber: Aggregate, logviewer-backend

logviewer-backend publish to redis chat.privmsg channel. (for notify self and another receiver (ex. aggregate, log viewers)
also publish to redis public.notify channel (for notify IRCBot) (that message like { message: "<nick> what" }

### channel.joined
* Publisher: IRC client
* Subscriber: logviewer-backend

### channel.parted
* Publisher: IRC client
* Subscriber: logviewer-backend
                
### irc.quit
* Publisher: IRC client
* Subscriber: logviewer-backend

### public.notify
* Publisher: logviewer-backend, Aggregate
* Subscriber: IRC client

--------------------------------------------------------------------------------

# socket.io Topic
## Server
### connect

## Client
### connect
### disconnect
###