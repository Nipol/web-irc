/**
 * Created by envi on 2014-05-05.
 */
var db = require('../db');
var dbconn = db.connect();

exports.is_exist_user_by_email = function (data, callback) {
    var usercheck_sql = {
        text: 'SELECT user_id::int FROM users WHERE email = $1',
        values: [ data.email ]
    };

    dbconn.query(usercheck_sql, function(err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(err, result.rowCount === 1);
        }
    });
};

exports.get_info_by_email = function (data, callback) {
    var usercheck_sql = {
        text: 'SELECT nickname, password, usertype FROM users ' + 
              'WHERE email = $1',
        values: [ data.email.toLowerCase() ]
    };

    console.log("Do query: %j", usercheck_sql);
    dbconn.query(usercheck_sql, function (err, results) {
        if (results.rowCount === 0) {
            callback(err, null);
        }
        else {
            callback(err, results.rows[0]);
        }
    });
};

exports.insert_user = function (data, callback) {
    var usersignup_sql = {
        text: 'INSERT INTO users (email, password, nickname, usertype, status, signup_date) ' + 
              'VALUES ($1, $2, $3, $4, $5, $6)',
        values: [
            data.email,
            data.password,
            data.nickname,
            'user', // "user" or "admin"
            0,
            new Date()
        ]
    };

    dbconn.query(usersignup_sql, function(err, results) {
        callback(err, results.count !== 0);
    });
};