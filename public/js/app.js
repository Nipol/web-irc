'use strict';

/**
 * Modules
 *
 * Modules Dependency Injection Set Up
 */
angular.module('common', []);
angular.module('mnemosyne.Home', ['ui.router', 'common']);
angular.module('mnemosyne.Signup', ['ui.router']);

/**
 * mnemosyne Module
 *
 * mnemosyne 베이스 모듈
 */
angular.module('mnemosyne', [
  'mnemosyne.Home',
  'mnemosyne.Signup',
  'ui.router'
]).

config(['$urlRouterProvider', function($urlRouterProvider) {
  // 기본 라우팅 위치
  // Otherwise, 특정 URL에 대한 Redirect도 여기서 지정한다.
  $urlRouterProvider
    .otherwise('/');
}]);