'use strict';

var signup = require('./controllers/signup');
var auth = require('./controllers/auth');

/**
 * Application routes
 * 
 * 구현 예
 * 모듈 형태로 구현 하여 route에 주입한다.
 * -auth.js
 * exports.workingName = function(req, res)
 * -routes.js
 * var auth = require('./controllers/auth');
 * app.route('/some-location').get(auth.workingName); 
 * app.route('/some-location').post(auth.workingName); 
 */
module.exports = function(app) {
  // signup
  app.route('/signup').post(signup.signup);
  // signin
  app.route('/auth/signin').post(auth.signin);
  // signout
  app.route('/auth/signout').post(auth.signout);

  // 지정되지 않은 api 호출에 대해 404.
  app.route('/api/*')
    .get(function(req, res) {
      res.send(404);
    });
};